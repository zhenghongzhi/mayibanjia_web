// module.exports = {
//     NODE_ENV: '"production"',
//     BASE_API: '"http://api1.ironforge.com.cn/v1/"',
//     BASE_URL: '"http://api1.ironforge.com.cn/"',
//     APP_ORIGIN: '"https://wallstreetcn.com"'
// };

module.exports = {
    NODE_ENV: '"production"',
    BASE_API: '"http://localhost:8000/v1/"',
    BASE_URL: '"http://localhost:8000/"',
    APP_ORIGIN: '"https://wallstreetcn.com"'
};