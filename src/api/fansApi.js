import fetch from 'utils/fetch';

//同步粉丝
var FansApi = {
    syncFans: function() {
        return fetch({
            url: '/fans/sync',
            method: 'get'
        })
    },
    //查询粉丝

    getFansList: function(pageSize, pageNo, searchText) {
        return fetch({
            url: '/fans/list?pageSize=' + pageSize + '&pageNo=' + pageNo + '&searchText=' + searchText,
            method: 'get'
        })
    }
}
export default FansApi;