import fetch from 'utils/fetch';
import Qs from 'qs';

var roleManager = {
    getList: function(pageSize, pageNo) {
        return fetch({
            url: '/role/list?pageSize=' + pageSize + '&pageNo=' + pageNo,
            method: 'get'
        })
    },
    addRole: function(data) {
        return fetch({
            url: '/role/add',
            method: 'post',
            data: data
        })
    },
    deleteRole: function(id) {
        return fetch({
            url: '/role/' + id,
            method: 'delete'
        })
    },
    editRole: function(data) {
        return fetch({
            url: '/role/' + data.id,
            method: 'put',
            data: data
        })
    },
    setMenu: function(data) {
        return fetch({
            url: '/role/' + data.id,
            method: 'post',
            data: data.data
        })
    },
    getRole: function(id) {
        return fetch({
            url: '/role/detail' + id,
            method: 'get',
        })
    }
}
export default roleManager;