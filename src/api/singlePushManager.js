import fetch from 'utils/fetch';
import Qs from 'qs';
var singlePushManager = {
    getList: function(pageSize, pageNo, keyword, data) {
        return fetch({
            url: '/singlepush/list?pageSize=' + pageSize + '&pageNo=' + pageNo + '&searchText=' + keyword,
            method: 'post',
            data: data
        })
    },
    add: function(data) {
        return fetch({
            url: '/singlepush/add',
            method: 'post',
            data: data
        })
    },
    edit: function(data) {
        return fetch({
            url: '/singlepush/' + data.id,
            method: 'put',
            data: data
        })
    },
    import: function(data) {
        return fetch({
            url: '/singlepush/import',
            method: 'post',
            data: data
        })
    },
    deleteUser: function(id) {
        return fetch({
            url: '/singlepush/delete?id=' + id,
            method: 'delete'
        })
    },
    push: function(id) {
        return fetch({
            url: '/singlepush/push?id=' + id,
            method: 'get'
        })
    },
    pushTwo: function(id) {
        return fetch({
            url: '/singlepush/pushTwo?id=' + id,
            method: 'get'
        })
    },
    getMember: function(pageSize, id) {
        return fetch({
            url: '/member/listByID?pageSize=' + pageSize + '&memberNo=' + id,
            method: 'get'
        })
    },
    getTmepalteList: function(pageSize, pageNo) {
        return fetch({
            url: '/template/list?pageSize=' + pageSize + '&pageNo=' + pageNo,
            method: 'get'
        })
    },
    deleteOneFile: function(id, type) {
        return fetch({
            url: '/upload/deleteFile?id=' + id + '&type=' + type,
            method: 'delete'
        })
    },
    getUploadFileList: function(id) {
        return fetch({
            url: '/upload/getUploadList?id=' + id + '&type=1',
            method: 'get'
        })
    },
    uploadCopyFile: function(id, fileStr) {
        var data = {
            id: id,
            type: 1,
            fileType: 1,
            upfile: fileStr
        };
        return fetch({
            url: '/upload/uploadBase64Img',
            method: 'post',
            data: data
        });
    },
    getTemplateMessage: function(id, tplID) {
        return fetch({
            url: '/message/content?msgID=' + id + '&msgType=1&tplID=' + tplID,
            method: 'get'
        })
    },
    getTemplateBindParam: function(id) {
        return fetch({
            url: '/template/getParam?tplID=' + id,
            method: 'get'
        })
    },
    newCopyPush: function(id) {
        return fetch({
            url: '/singlepush/' + id,
            method: 'post'
        })
    }

}

export default singlePushManager;