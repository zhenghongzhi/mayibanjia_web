import fetch from 'utils/fetch';
import Qs from 'qs';
var templateMessage = {
    getList: function(pageSize, pageNo) {
        return fetch({
            url: '/template/list?pageSize=' + pageSize + '&pageNo=' + pageNo,
            method: 'get'
        })
    },
    searchList: function(pageSize, pageNo, data) {
        return fetch({
            url: '/template/list?pageSize=' + pageSize + '&pageNo=' + pageNo + '&searchText=' + data,
            method: 'get',
        })
    },
    syncList: function() {
        return fetch({
            url: '/template/sync',
            method: 'get'
        })
    },

    updateList: function(body) {
        return fetch({
            url: '/template/update',
            method: 'put',
            data: body,
        })
    }

}
export default templateMessage;