import fetch from 'utils/fetch';
import Qs from 'qs';

var userManager = {
    getList: function(pageSize, pageNo) {
        return fetch({
            url: '/user/list?pageSize=' + pageSize + '&pageNo=' + pageNo,
            method: 'get'
        })
    },
    addUser: function(data) {
        return fetch({
            url: '/user/add',
            method: 'post',
            data: data
        })
    },
    deleteUser: function(id) {
        return fetch({
            url: '/user/' + id,
            method: 'delete'
        })
    },
    editUser: function(data) {
        return fetch({
            url: '/user/' + data.id,
            method: 'post',
            data: data
        })
    },
    restPwd: function(id) {
        const data = { id };
        //以form-data方式发起请求
        return fetch({
            url: '/account/resetPwd',
            method: 'post',
            data: Qs.stringify(data)
        })
    },
    setRole: function(data) {
        return fetch({
            url: '/user/setrole/' + data.userID,
            method: 'post',
            data: Qs.stringify(data)
        })
    }
}
export default userManager;