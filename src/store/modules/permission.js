import { asyncRouterMap, constantRouterMap } from 'src/router';

/**
 * 通过meta.role判断是否与当前用户权限匹配
 * @param roles
 * @param route
 */
function hasPermission(menuIds, route) {
    if (route.id) {
        return menuIds.some(x => (x == route.id || x.toString().substring(0, 1) == route.id));
    }
    return false;
}


/**
 * 递归过滤异步路由表，返回符合用户角色权限的路由表
 * @param asyncRouterMap
 * @param roles
 */
function filterAsyncRouter(asyncRouterMap, menuIds) {
    const accessedRouters = asyncRouterMap.filter(route => {

        if (hasPermission(menuIds, route)) {
            console.log(route);
            if (route.children && route.children.length) {
                route.children = filterAsyncRouter(route.children, menuIds)
            }
            return true
        }
        return false
    })
    return accessedRouters
}

const permission = {
    state: {
        routers: constantRouterMap,
        addRouters: []
    },
    mutations: {
        SET_ROUTERS: (state, routers) => {
            state.addRouters = routers;
            state.routers = constantRouterMap.concat(routers);
        }
    },
    actions: {
        GenerateRoutes({ commit }, userroles) {
            return new Promise(resolve => {
                let accessedRouters
                if (userroles.some(x => x.role.name == 'admin')) {
                    accessedRouters = asyncRouterMap;
                } else {
                    var menuIds = [];
                    userroles.forEach(x => {
                        menuIds = menuIds.concat(x.role.menus)
                    });
                    console.log(menuIds)
                    accessedRouters = filterAsyncRouter(asyncRouterMap, menuIds)
                }
                commit('SET_ROUTERS', accessedRouters);
                resolve();
            })
        }
    }
};

export default permission;